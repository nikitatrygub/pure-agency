import Artboard from "./containers/Artboard";

export const routes = [
  {
    exact: true,
    path: "/",
    component: Artboard
  },
  {
    exact: true,
    path: "/board2",
    component: Artboard
  },
  {
    exact: true,
    path: "/board3",
    component: Artboard
  }
];
