import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class ArtboardPage extends Component {
  onDragOver = e => {
    e.preventDefault();
  };
  onDragStart = (e, id) => {
    e.dataTransfer.setData("id", id);
  };
  onDrop = (e, type) => {
    let id = e.dataTransfer.getData("id");

    let items = this.props.ArtboardPage.filter(item => {
      if (item.title === id) {
        item.part = type;
      }
      return item;
    });
    this.props.updateItems(items);
  };
  render() {
    const { ArtboardPage } = this.props;

    const ItemsTypes = {
      left: [],
      right: []
    };

    ArtboardPage.forEach(item => {
      ItemsTypes[item.part].push(
        <div
          key={item.title}
          onDragStart={e => this.onDragStart(e, item.title)}
          draggable
          className="item"
        >
          {item.title}
        </div>
      );
    });

    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <div
              className="left-block"
              onDragOver={e => this.onDragOver(e)}
              onDrop={e => this.onDrop(e, "left")}
            >
              <div className="title">Left block</div>
              {ItemsTypes.left}
            </div>
          </div>
          <div className="col-6">
            <div
              className="right-block"
              onDragOver={e => this.onDragOver(e)}
              onDrop={e => this.onDrop(e, "right")}
            >
              <div className="title">Right block</div>
              {ItemsTypes.right}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(ArtboardPage);
