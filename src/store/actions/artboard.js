export const UPDATE_ITEMS = "UPDATE_ITEMS";

export const updateItemsAction = items => ({
  type: UPDATE_ITEMS,
  payload: items
});
