import { UPDATE_ITEMS } from "../actions/artboard";

const initialState = [
  {
    title: "Item 1",
    part: "left"
  },
  {
    title: "Item 2",
    part: "left"
  },
  {
    title: "Item 3",
    part: "left"
  },
  {
    title: "Item 4",
    part: "left"
  }
];

export function artboard(state = initialState, action) {
  switch (action.type) {
    case UPDATE_ITEMS: {
      let newState = action.payload;
      return newState;
    }
  }
  return state;
}
