import { connect } from "react-redux";
import ArtboardPage from "../pages/ArtboardPage";
import { bindActionCreators } from "redux";
import { updateItemsAction } from "../store/actions/artboard";

const mapStateToProps = ArtboardPage => {
  return {
    ArtboardPage
  };
};

const mapDispatchToState = dispatch => ({
  updateItems: bindActionCreators(updateItemsAction, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToState
)(ArtboardPage);
