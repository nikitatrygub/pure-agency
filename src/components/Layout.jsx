import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const Layout = ({ children }) => (
  <Fragment>
    <div className="header">
      <ul className="header__nav">
        <li>
          <Link to="/">Board 1</Link>
        </li>
        <li>
          <Link to="/board2">Board 2</Link>
        </li>
        <li>
          <Link to="/board3">Board 3</Link>
        </li>
      </ul>
    </div>
    {children}
  </Fragment>
);

export default Layout;
