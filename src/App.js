import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes } from "./routes";
import Layout from "./components/Layout";

class App extends Component {
  render() {
    return (
      <Router>
        <Layout>
          <Switch>
            {routes.map((route, index) => (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            ))}
          </Switch>
        </Layout>
      </Router>
    );
  }
}

export default App;
